import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Dimensions } from 'react-native';
import * as ScreenOrientation from 'expo-screen-orientation';

export default function App() {
  const [isActive, setIsActive] = useState(false);
  const [color, setColor] = useState('white');
  const [orientation, setOrientation] = useState(ScreenOrientation.Orientation.UNKNOWN);
  const [screenWidth, setScreenWidth] = useState(Dimensions.get('window').width);
  const [screenHeight, setScreenHeight] = useState(Dimensions.get('window').height);

  useEffect(() => {
    const updateLayout = async () => {
      try {
        const orientationInfo = await ScreenOrientation.getOrientationAsync();
        const { width, height } = Dimensions.get('window');
        setOrientation(orientationInfo);
        setScreenWidth(width);
        setScreenHeight(height);
      } catch (error) {
        console.error("Failed to get orientation:", error);
      }
    };

    updateLayout(); // Initial layout update

    const subscription = ScreenOrientation.addOrientationChangeListener(({ orientationInfo }) => {
      updateLayout();
    });

    return () => {
      ScreenOrientation.removeOrientationChangeListener(subscription);
    };
  }, []);

  useEffect(() => {
    let interval = null;
    if (isActive) {
      interval = setInterval(() => {
        setColor(prevColor => (prevColor === 'white' ? 'red' : 'white'));
      }, 500); // Change color every 500ms
    } else {
      clearInterval(interval);
      setColor('white'); // Reset color to white when not active
    }
    return () => {
      clearInterval(interval);
    };
  }, [isActive]);

  const textColor = color === 'white' ? 'red' : 'white';
  const emergencyFontSize = screenWidth * 0.1; // 10% of screen width
  const textWidth = orientation === ScreenOrientation.Orientation.LANDSCAPE_LEFT || orientation === ScreenOrientation.Orientation.LANDSCAPE_RIGHT ? screenWidth * 0.9 : screenWidth * 0.8; // Adjust text width based on orientation

  const rotationStyle = orientation === ScreenOrientation.Orientation.LANDSCAPE_LEFT || orientation === ScreenOrientation.Orientation.LANDSCAPE_RIGHT ? '0deg' : '90deg';

  return (
    <View style={[styles.container, { backgroundColor: color }]}>
      {isActive && (
        <Text style={[styles.emergencyText, {
          color: textColor,
          fontSize: emergencyFontSize,
          width: textWidth,
          letterSpacing: 2
        }]}>EMERGENCY</Text>
      )}
      <TouchableOpacity style={isActive ? styles.stopButton : styles.startButton} onPress={() => setIsActive(!isActive)}>
        <Text style={[styles.startText, { fontWeight: 'bold', color: textColor }]}>
          {isActive ? 'STOP WARNING' : 'START EMERGENCY WARNING'}
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  emergencyText: {
    fontWeight: 'bold',
    textAlign: 'center',
    letterSpacing: 2,
  },
  startButton: {
    position: 'absolute',
    bottom: '50%',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  stopButton: {
    position: 'absolute',
    bottom: 20,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  startText: {
    fontSize: 16,
  },
});

